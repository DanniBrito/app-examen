//
//  ciudadTierra.swift
//  App Examen BG
//
//  Created by Danni Brito on 12/12/17.
//  Copyright © 2017 Danni Brito. All rights reserved.
//

import Foundation
import ObjectMapper

class WeatherResponse: Mappable {
    var name: String?
    var weather: [Forecast]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        weather <- map["weather"]
    }
}

class Forecast: Mappable {
    var clima: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        clima <- map["description"]
    }
}
