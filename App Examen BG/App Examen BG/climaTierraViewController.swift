//
//  climaTierraViewController.swift
//  App Examen BG
//
//  Created by Danni Brito on 12/12/17.
//  Copyright © 2017 Danni Brito. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class climaTierraViewController: UIViewController {

    @IBOutlet weak var ciudadBuscar: UITextField!
    
    @IBOutlet weak var resultado: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func consultarButtonPressed(_ sender: Any) {
        
        let buscar = ciudadBuscar.text!
        let URL = "http://api.openweathermap.org/data/2.5/weather?q=\(buscar)&appid=498e30a73cd9cd3f1b86bb3e7bb2b4f5"
        Alamofire.request(URL).responseObject { (response: DataResponse<WeatherResponse>) in
            
            let weatherResponse = response.result.value
            
            
                DispatchQueue.main.async {
                    self.resultado.text = "\(weatherResponse?.weather![0].clima ?? " ")"
                }
            
        }
        
        
        
    }
    
    
    

}
