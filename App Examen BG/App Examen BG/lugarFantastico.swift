//
//  lugarFantastico.swift
//  App Examen BG
//
//  Created by Danni Brito on 12/12/17.
//  Copyright © 2017 Danni Brito. All rights reserved.
//

import Foundation
import ObjectMapper

class heroe: Mappable {
    var name: [infoNombre]?
    var imagen: [infoImagen]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        name <- map["results"]
        imagen <- map["results"]
    }
}

class infoNombre: Mappable {
    var nombreReal: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        nombreReal <- map["real_name"]
    }
}

class infoImagen: Mappable {
    var imagenF: String?
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        imagenF <- map["image.icon_url"]
    }
}
