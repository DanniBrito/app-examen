//
//  lugaresViewController.swift
//  App Examen BG
//
//  Created by Danni Brito on 12/12/17.
//  Copyright © 2017 Danni Brito. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class lugaresViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource{

    @IBOutlet weak var nombre: UILabel!
    @IBOutlet weak var imagen: UIImageView!
    
    @IBOutlet weak var buscarTextView: UITextField!
    let datos = ["Batman","Superman","Deadpool","Aquaman","Punisher","Venom","Mandarin","Deathstroke","Penguin","Joker","Thanos","Antman","Quicksilver","Goku"]
    
    var fila:Int = 1
    
    override func viewDidLoad() {
        
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        var pickerView = UIPickerView()
        
        pickerView.delegate = self
        
        buscarTextView.inputView = pickerView
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return datos.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return datos[row]
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        fila = row
        buscarTextView.text = datos[row]
        
        
        
    }
    
    func downloadImage(_ uri : String, inView: UIImageView){
        
        let url = URL(string: uri)
        
        let task = URLSession.shared.dataTask(with: url!) {responseData,response,error in
            if error == nil{
                if let data = responseData {
                    
                    DispatchQueue.main.async {
                        inView.image = UIImage(data: data)
                    }
                    
                }else {
                    print("no data")
                }
            }else{
                print(error ?? nil)
            }
        }
        
        task.resume()
        
    }
    
    @IBAction func buscarButtonPressed(_ sender: Any) {
        let URL:String = "http://comicvine.gamespot.com/api/characters/?api_key=a1fcd12b8fc64f0a8d726928daf38e49b39b2d4d&format=json&limit=1&filter=name:\(datos[fila])"
        Alamofire.request(URL).responseObject { (response: DataResponse<heroe>) in
            
            let respuesta = response.result.value
            
            
            DispatchQueue.main.async {
                self.nombre.text = "\(respuesta?.name![0].nombreReal ?? " ")"
                print("\(respuesta?.imagen![0].imagenF ?? " ")")
                print(URL)
                self.downloadImage("\(respuesta?.imagen![0].imagenF ?? " ")"
, inView: self.imagen)
                print("\(respuesta?.imagen![0].imagenF ?? " ")")
            }
            
        }
        
        
        
    }
    

}
