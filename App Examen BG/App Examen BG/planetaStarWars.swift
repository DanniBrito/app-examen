//
//  planetaStarWars.swift
//  App Examen BG
//
//  Created by Danni Brito on 11/12/17.
//  Copyright © 2017 Danni Brito. All rights reserved.
//

import Foundation
import ObjectMapper

class planetaStarWars: Mappable{
    var name:String?
    var clima:String?
    
    required init (map: Map){
        
    }
    
    func mapping(map:Map){
        name <- map["name"]
        clima <- map["climate"]
    }
    
    
}

class mundos: Mappable {
    var results: [mundoDetalle]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        results <- map["results"]
    }
}

class mundoDetalle: Mappable {
    var name: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        name <- map["name"]
    }
}
