//
//  starWarsViewController.swift
//  App Examen BG
//
//  Created by Danni Brito on 11/12/17.
//  Copyright © 2017 Danni Brito. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class starWarsViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

    
    
    @IBOutlet weak var climaConsultado: UILabel!
    
    @IBOutlet weak var busquedaTextView: UITextField!
    
    
    var pickerData = ["Tierra","Alderaan","Yavin IV","Hoth","Dagobah","Bespin","Endor","Naboo","Coruscant","Kamino","Geonosis"]
    
    
    
    var planeta = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        var pickerView = UIPickerView()
        
        pickerView.delegate = self
        
        busquedaTextView.inputView = pickerView
        
        
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    //obtener el valor del pickerView
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        planeta = row + 1
        
        Alamofire.request("https://swapi.co/api/planets/\(planeta)").responseObject{(response: DataResponse<planetaStarWars>) in
            
            let planeta = response.result.value
            DispatchQueue.main.async {
                self.climaConsultado.text = planeta?.clima ?? " "
            }
            
        }
        busquedaTextView.text = pickerData[row]
        
    }
    
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        
        let string = pickerData[row]
        return NSAttributedString(string: string, attributes: [NSAttributedStringKey.foregroundColor:UIColor.white])
    }
    
    
    
    
    
    
    

}
